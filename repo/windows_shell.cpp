#include <iostream>
#include <string>
#include <Windows.h>
#include "Helper.h"
#define PATH_MAX 2048
#define CREATE seif4

using namespace std;

void seif5(vector<string> inputWords)
{

}

void seif4(vector<string> inputWords)
{
	HANDLE hFile;
	if (inputWords[0] == "create" && inputWords.size() > 1)
	{
		hFile = CreateFile((inputWords[1]).c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			cout << (TEXT("Terminal failure: unable to open file \"%s\" for read.\n"), (inputWords[1]).c_str());
		}
	}
}

void seif3(vector<string> inputWords)
{
	int rc = 0;
	if (inputWords[0] == "cd" && inputWords.size() > 1)
	{
		if (!SetCurrentDirectory((inputWords[1]).c_str()))
		{
			cout << "ERROR: Setting Directory Failed" << endl;
		}
	}
}


void seif2(string str)
{
	TCHAR pwd[PATH_MAX];

	GetCurrentDirectory(MAX_PATH, pwd);
	if (!str.compare("pwd"))
	{
		cout << pwd << endl << endl;
	}
}

void seif1()
{
	string inputFromUser;
	vector<string> inputWords;

	getline(cin , inputFromUser);

	Helper::trim(inputFromUser);
	inputWords = Helper::get_words(inputFromUser);

	seif2(inputWords[0]);
	seif3(inputWords);
	seif4(inputWords);
}

int main()
{
	while (1)
	{
		cout << ">>";
		seif1();
	}
	return 0;
}

